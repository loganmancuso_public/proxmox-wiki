### Testing 
testing locally run `gitlab-runner exec docker pages`

create local environment
```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
mkdocs serve --dev-addr 0.0.0.0:4001
```

or run using docker
`docker-compose up --build`

Access the published wiki:
- [Wiki](https://loganmancuso_public.gitlab.io/proxmox-wiki)
- `https://loganmancuso_public.gitlab.io/proxmox-wiki`
