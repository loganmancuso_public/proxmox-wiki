---
type: standard
---
## 💽 Proxmox Resource ID's

Proxmox uses a 32bit unsigned integer (9 digit) ID to distinguish resources. This outlines the basic convention to use when identifying a resource. By having a prefix for each resource it makes identifying it in the console much easier since you cannot manually group similar resources outside of the pre-made views in the proxmox console. 

### Keeping Templates in sync

I have written a helper script to assist in deploying new lxc and machine templates. There are so many revisions and versions that need to be kept in sync. This bundles the revisions of latest and stable and allows for simultaneous deployments for revisions. To use is just pass the required parameters to the script. 

```bash
# to keep all os, versions and revisons for a given environment
tf-template --env app --action plan
tf-template --env app --action apply

# to keep only ubuntu versions and revisions in sync
tf-template --env app --action plan --os ubuntu
tf-template --env app --action apply --os ubuntu
```

#### LXC
```bash
## infra ##
tf-template --env infra --action 'destroy' --os debian --version bookworm-1207 --revision stable
tf-template --env infra --action 'apply --auto-approve' --os debian --version bookworm-1207 --revision stable

## app ##
# lxc 
tf-template --env app --action 'destroy' --os debian --version bookworm-1207 --revision stable
tf-template --env app --action 'apply --auto-approve' --os debian --version bookworm-1207 --revision stable

tf-template --env app --action 'destroy' --os ubuntu --version noble-2404 --revision stable
tf-template --env app --action 'apply --auto-approve' --os ubuntu --version noble-2404 --revision stable

```

#### Machines
```bash
## infra ##
tf-template --env app --action 'destroy' --os ubuntu --version noble-2404 --revision stable
tf-template --env app --action 'apply --auto-approve' --os ubuntu --version noble-2404 --revision stable

## app ##
tf-template --env app --action 'destroy' --os ubuntu --version noble-2404 --revision stable
tf-template --env app --action 'apply --auto-approve' --os ubuntu --version noble-2404 --revision stable

```

| Resource      | Prefix | OS ID | OS Version Major | OS Minor Version | Revision |
| ------------- | ------ | ----- | ---------------- | ---------------- | -------- |
| LXC Template  | 10     |       |                  |                  |          |
| LXC           | 11     |       |                  |                  |          |
| Templates     |        |       |                  |                  |          |
| Ubuntu Stable | 10     | 01    | 24               | 04               | 0        |
| Ubuntu Latest | 10     | 01    | 24               | 04               | 1        |
| Debian Stable | 10     | 02    | 12               | 07               | 0        |
| Debian Latest | 10     | 02    | 12               | 07               | 1        |
|               |        |       |                  |                  |          |
|               |        |       |                  |                  |          |
|               |        |       |                  |                  |          |

| Resource         | Prefix | OS ID | OS Version Major | OS Minor Version | Revision |
| ---------------- | ------ | ----- | ---------------- | ---------------- | -------- |
| Machine Template | 20     |       |                  |                  |          |
| Machine          | 21     |       |                  |                  |          |
| Templates        |        |       |                  |                  |          |
| Ubuntu Stable    | 20     | 01    | 24               | 04               | 0        |
| Ubuntu Latest    | 20     | 01    | 24               | 04               | 1        |

| Resource | Prefix | ID  |
| -------- | ------ | --- |
| Sentry   | 11     | 10  |
| Thoth    | 21     | 10  |
| Ingress  | 21     | 100 |
| Utility  | 21     | 110 |

## 🖥️ VM Template Requirements

### 📦 Packages:
- python3
- docker
- lazydocker
- wget
- curl
- git
- opentelemetry
- loki log driver
### 📂 Directories
- /var/log/tofu
	- this will be the root logging folder for all tofu related scripts
- /opt/tofu
	- root folder for bootstrap script and other dependent scripts. 
### 🔑 Permissions/Scope
- the default user
	- part of the docker group
	- owner/group of /opt/tofu, /var/log/tofu
