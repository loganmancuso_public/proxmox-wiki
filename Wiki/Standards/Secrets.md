---
type: standard
---
## 🔒 KV2 Paths

| Path   | Usage                                                         |
| ------ | ------------------------------------------------------------- |
| Shared | This is a vault for secrets that are shared between workflows |
| Infra  | This is reserved for secrets used to configure infrastructure |
| App    | This is a vault for application credentials                   |
