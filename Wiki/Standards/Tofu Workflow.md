---
type: standard
---
## 📁 Directory

Each project has the same basic folder structure. The root of the project is where the automation code sits. There is an env folder that contains the main.tf and terraform.tfvars. Simply put your main.tf will manage your state file, and terraform.tfvars will be your input variables. The required variables are defined in the variables.tf. The env parameter in the terraform.tfvars is used to manage a logical grouping of nodes. This name will be shared across all backend state files. It provides a unique workspace to separate out your environment. 

```text
| project_name/
|--- env/
|   |--- main.tf
|   |--- terraform.tfvars
|--- templates/
|--- scripts/
|   |--- bootstrap.sh
|--- imports.tf
|--- provider.tf
|--- outputs.tf
```

### 📃 Required Files
- `imports.tf`
	- this file will consume all upstream state file variables required to deploy the infrastructure.
- `outputs.tf`
	- this will publish any resource details to the state file for downstream workflows.
- `provider.tf`
	- this will manage the provider configuration for the workflow, allowing the main.tf to act as the backend configuration
- `env/main.tf`
	- this will construct the state file to write the environment infrastructure to. 
- `env/terraform.tfvars`
	- this will hold variables and unique values for each independent deployment of the workflow. 

### 🔳 Resources & Data Blocks
- delimited by an underscore
- should be descriptive and follow the resource name from the provider
#### 🔤 Variables 
- delimited by an underscore
#### 🔃 Outputs and Imports
- delimited by an underscore
- should include a prefix
	- global secrets:
		- secret_{variable} > credentials_{variable}
	- datacenter infrastructure:
		- network_id_{subnet} > network_{subnet}

