---
type: mentions
---
## 👋🏼 [About Me](https://loganmancuso.github.io/)
Author: [Logan Mancuso](https://gitlab.com/loganmancuso) [Contact Me](https://loganmancuso.github.io)
I am an IaC engineer with a passion or automation, and open source projects. Primarily working with GoLang and OpenTofu, and Kubernetes.

---

Thanks to FOSS, and these projects we can enjoy the benefits of this software. This project would not be possible with out the hard work of these people listed below. Thank you, and please consider sponsoring these creators.

---

<p style="text-align: center;">If I have seen further than others, it is by standing upon the shoulders of giants.</p>
<H5 style="text-align: center;">Isaac Newton</H5>

---

## 🙏 [Proxmox Provider](https://github.com/bpg/terraform-provider-proxmox/tree/main)
Owner: [Pavel Boldyrev](https://github.com/bpg)
This project interfaces with the proxmox api allowing us to use tofu to provision infrastructure on proxmox. I have personally used this to automate the deployment of nodes in a proxmox cluster as well as provision instances with prebuilt images and custom bootstrapping. This provider here is really the backbone of this entire project, it is still actively maintained and is a thriving community please consider participating. 😄  

## 🙏 [Packer Template](https://github.com/ChristianLempa/homelab/tree/main/proxmox)
Owner: [Christian Lempa](https://github.com/ChristianLempa) 
The inspiration for the start of this whole project I advise you to checkout his video [here](https://www.youtube.com/watch?v=1nf3WOEFq1Y&t=1143s). He goes over how the packer script works. The project [[Infrastructure/Template Machine|Template Machine]] was forked from this code and modified into a tofu workflow for completely automated deployments. 

## 🙏 [Libvirt Provider](https://github.com/dmacvicar/terraform-provider-libvirt)
Owner: [Duncan Mac-Vicar P.](https://github.com/dmacvicar)
This project interfaces with the libvirt api, allowing me to setup a dynamic development environment with a rapidly deployable local proxmox instance. 