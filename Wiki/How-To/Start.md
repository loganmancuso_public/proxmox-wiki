## 🚧 Assumptions

1. A machine running a fresh install of Proxmox 8 or newer. 
2. OpenTofu, Docker, and Packer are installed locally on your development machine (not the Proxmox node). 
3. You have a local network configured with at least one subnet and you are able to ssh into the existing Proxmox machine.
4. A gitlab account with api key for accessing remote state storage and module storage.
## 🟢 Steps

### Local Infrastructure and Secrets
1. [[../Standards/Tofu Workflow|Tofu Workflow]]: You will need to set 3 environment variables in order for Tofu to authenticate with the remote state storage. The document also goes over how to use bitwarden to automate this process however, just setting the environment variables will be enough. 
2. [[../Applications/Vault]]: This is the first project you will need to deploy. Fork the code base and modify the main.tf to point to your forked project ID.
	- **do not forget to initialize the vault**
3. [[Development Workflow|Workflow/Vault Variables]]: Now that the vault is initialized you will need to set your root token and unseal key environment variables. 
	- when logging out of a session or rebooting your machine the vault will seal itself. You will need to unseal the vault before deploying infrastructure
4. [[Infrastructure/Global Secrets|Global Secrets]]: fork this workflow, it contains all of your secrets at rest. this includes authentication to the proxmox cluster. You will need to modify the import.tf to point to the vault state file you have just deployed. There are a lot of variables and most of them you may not need, feel free to delete any unnecessary secrets from the vault.

### Proxmox Infrastructure

1. [[Infrastructure/Datacenter|Datacenter]]: fork this repository and update the imports.tf with the corresponding state files as well. This workflow is designed to be deployed against a clean install of proxmox. None of my testing included existing deployments. 

At this point you can either deploy my machine templates or create your own. I currently only have an Ubuntu server configured but I plan to setup a deploy for Arch or maybe a NixOs later on. 

#### Machine Deployments

1. [[Infrastructure/Template Machine|Template Machine]] and [[Infrastructure/Template LXC|Template LXC]] fork these workflows and update the imports.tf to match the above steps. 
2. [[Infrastructure/Module Machine|Module Machine]] and [[Infrastructure/Module LXC|Module LXC]] fork these workflows, update the imports.tf and then deploy. 