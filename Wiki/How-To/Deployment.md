---
type: how-to
---
## 🚧 Steps
1. You'll need a system with [docker](https://docs.docker.com/engine/install/) (or equivalent like [podman](https://podman.io/docs/installation)) installed to host your local vault; start with [this project](https://gitlab.com/loganmancuso_public/infrastructure/applications/vault). This will deploy a vault to house all of your secrets at-rest through the automation.
2. Deploy the [global secrets](https://gitlab.com/loganmancuso_public/infrastructure/proxmox/global-secrets) automation to populate all of the secrets to manage and control your infrastructure.
3. This deployment sets up the [datacenter](https://gitlab.com/loganmancuso_public/infrastructure/proxmox/datacenter); if you are familiar with AWS, think of this as the VPC. **Ensure you have a backup of your existing Proxmox instance** I would recommend creating a libvirt VM of Proxmox and testing all of the code out on that instance before attempting to do this on your production server.
4. From this point, you are done with the infrastructure and can now focus on creating [virtual machine templates](https://gitlab.com/loganmancuso_public/infrastructure/proxmox/machine-templates). If you are familiar with AWS, think of this as creating an AMI.
5. With an instance template deployed, you are now able to deploy [instances](https://gitlab.com/loganmancuso_public/infrastructure/proxmox/instances/sandbox) on Proxmox. Each deployment will have its own security group rules, cloud-init template, and its own bootstrapping capabilities through a helper script.

🚩 you may want to read [[../Standards/Tofu Workflow|Tofu Workflow]] to get familiar with the folder structure of my code before deploying the first workflow. 

All of the code can be found here: https://gitlab.com/loganmancuso_public/infrastructure
### 🔄 Dependency Tracking
These workflows imports.tf have a lot of values pulled from multiple sources. This is a diagram to understand the dependency and workflow precedence.
```mermaid
flowchart TD
	subgraph Local
		subgraph Vault
			vault[container for secrets management] 
		end
		subgraph GlobalSecrets
			gs[store secrets for automation]
		end
	end
	subgraph Proxmox-Dev
		subgraph LocalInfrastructure
			li[libvirt network configuration]
		end
		subgraph DevDatacenter
			proxmoxdev[development libvirt vm for proxmox]
		end
	end
	subgraph ProxmoxDatacenter
		subgraph DatacenterInfrastructure
			dc[backbone of the proxmox cluster]
		end
		subgraph TemplateLXC
			tofu[lxc images]
		end
		subgraph ModuleLXC
			tofulxc[lxc module deploy]
		end
		subgraph TemplateMachine
			packer[machine images]
		end
		subgraph ModuleMachine
			tofumachine[machine module deploy]
		end
		subgraph SandboxInstance
			sbx[a template instance to customize]
		end
	end
	subgraph Applications
		subgraph ApplicationTemplate
			app[template for deploying a container to an instance]
		end
	end
	Vault --> GlobalSecrets
	GlobalSecrets --> DatacenterInfrastructure
	GlobalSecrets --> DevDatacenter
	LocalInfrastructure --> DevDatacenter
	DatacenterInfrastructure --> TemplateMachine
	DatacenterInfrastructure --> TemplateLXC
	GlobalSecrets --> TemplateMachine
	GlobalSecrets --> TemplateLXC
	TemplateMachine --> ModuleMachine
	TemplateLXC --> ModuleLXC
	DatacenterInfrastructure --> SandboxInstance
	GlobalSecrets --> SandboxInstance
	ModuleMachine --> SandboxInstance
	SandboxInstance --> ApplicationTemplate
```


<<[[Tooling]] --- [[Applications/Vault|Vault]]>>