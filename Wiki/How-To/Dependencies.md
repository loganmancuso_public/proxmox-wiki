---
type: how-to
---
## 🖱️ Technical Dependencies
- OpenTofu or Terraform installed with a remote state storage such as [gitlab](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)
- A Linux virtual machine or computer with ssh and api access to the current Proxmox server
- Nice-to-have: Virtual deployment of Proxmox where you can do lab testing before deploying this to Production.
### 👍 Good to Know 
- Comfortable with networking such as firewalls, VLAN, security groups, DNS, network interfaces.
- A thorough background in IaC comfortable with the command line, as automation relies heavily on cli (Command Line Interface).
- Understanding of api's (at least how to deconstruct a payload). 
- Enthusiasm, It should be fun, IaC is all about repeatability. I've spent hours destroying my work to re-apply it for testing. IaC's advantage is front-loading the work. Do it all today, and make it repeatable, so you don't have to remember how to do it in the future.

*note* over the course of this project I have broken proxmox in every conceivable way, this includes reinstalling the OS from scratch about 50+ times already. This automation can really save you some time bootstrapping a node.  

### 🧭 Overview
This is the architecture of the environment we are going to want to construct:

```mermaid
flowchart LR
	subgraph Localhost
		subgraph Docker
			Vault
		end
		subgraph ProxmoxKVM
			Packer
		end
		OpenTofu
		ssh
	end
	subgraph Datacenter
		Packer
		ProxmoxAPI
		ProxmoxCLI
		ssh
	end
	OpenTofu --> ProxmoxAPI
	OpenTofu --> ssh --> ProxmoxCLI
	OpenTofu --> Packer
	
```

This setup has 2 physical machines, a datacenter and localhost. The localhost acts as the deployment server and remotely executes commands on the datacenter in a few ways. The primary method is the bgp tofu provider, this uses the proxmox api and will be the most configurable. As great as the provider is, there are some features that rely on other methods to communicate, in this case I fall back to ssh and interact with the datacenter using the proxmoxcli, qemu/lxc, and native bash scripting.  There is an additional tool, Packer, that is installed as part of the datacenter workflow. Packer is used in the template-machine workflow to create a qemu vm from a raw iso file. It uses packer and cloud init to create the basic template with all the custom bootstrapping. 

<<[[IaC]] --- [[Tooling]]>>