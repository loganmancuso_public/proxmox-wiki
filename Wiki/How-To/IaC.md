---
type: overview
---
## 🔍 What is IaC?
Infrastructure as Code (IaC) is a method of managing and provisioning infrastructure through machine-readable definition files, rather than through manual processes. It uses DevOps methodology and a code versioning model to deploy infrastructure such as: networks, virtual machines, load-balancer's, and much more. Just as the same source code always generates the same binary, an IaC model generates the same environment every time it deploys. This approach helps avoid manual configuration, enforces consistency, and prevents runtime issues caused by configuration drift or missing dependencies.

## 🔍 What is OpenTofu
OpenTofu is a fork of a Hashicorp product, Terraform, and allows for the deployment of Infrastructure through a unique life-cycle management process. Like a database, Tofu operates through a set of three stages: Write, Plan, and Apply. This capability allows a user to manage complex infrastructure over the course of its lifetime, evolving to suite your needs. 
### ♻️ Stages:
- Write: Define, in code, all of your resources and data that you want to exist in your infrastructure. 
- Plan: Tofu will create an execution plan for the underlying infrastructure, with a description of any resources it will create, destroy, or update.
- Apply: After approving the plan, Tofu will orchestrate the deployment of the resources, respecting dependencies. This *state* is then written to persistent storage. 

---
### 🚧 Requirements
When setting out to start this project I wanted to keep a few requirements in mind. 

1. Free - A lot of this work is based on Free and Open Source Software (FOSS), please consider sponsoring some of these projects in the [[Mentions]] page. FOSS is a fundamental part of the development community. I hope that these projects, and this Wiki, help contribute to others as these projects have helped me. 
2. Scaleable - Most people do not have multiple servers sitting around ready to be clustered together. I am limited to a single server and a localhost for machines. My goal for this project is to build code in a way that will scale with the infrastructure. 
3. Maintanable - Hosting applications at home can be a pain because we all **"just want it to work"**. Using Tofu makes this whole process much easier by storing all of your knowledge, custom configuration, and steps in code. I cannot count the number of times I have solved a problem just for it to come back six months later and have to figure it all out again. IaC eliminates that problem by always compiling to the same binary state. 
4. Fast - Sometimes I just need a quick way to deploy a test instance, to try out something and have it be destroyed a few hours later when I am done with it. Having the backbone of Proxmox and all my dependent states managed in Tofu allows me to configure an instance quickly for testing. 


<<[[index|Introduction]] --- [[Dependencies]]>>