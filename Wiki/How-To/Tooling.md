---
type: how-to
---
## 🌄 Overview
There are a few components that are prerequisites to the deployment of these workflows as they are, with minimal code changes: [[Applications/Vault|Vault]], and [GitLab remote state files](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html).

Remote State storage with tofu is not required but it will make this entire process a lot easier. GitLab offers free remote state storage and until I can deploy my own GitLab server locally this is the best free solution. There is an alternative that is explained more below. 

Deploying vault is a difficult process, especially when you want to run a production version of it locally. By default the container is designed to be run in a development setting and does not persist anything to disk. I built a tofu workflow around deploying it. The workflow automatically creates the certs required to secure vault and downloads them to your localhost for storage.

![[../assets/images/workflow.png]]
### 🔐 [[Applications/Vault|Vault]]
You will need a way to store secrets in your workflows to better protect information as it runs through your automation. My solution was to use Hashicorp Vault, but if you have an Azure keyvault, or an AWS KMS, you can use either of those solutions. In trying to align with my no-cost solution, I chose to implement a local docker deployment of Vault.
### ♻️ [OpenTofu](https://opentofu.org/)
Tofu will act as the orchestration engine of the infrastructure. It will take the workflow, compile to a binary state and execute the changes on approval. All of this is run from your localhost and will require that you have network connectivity to the api of your target Proxmox instance over tcp/8006.
### 🦮 [[Infrastructure/Template Machine|Template Machine]]
Packer is another tool that will be used to create custom images. It is set to use port tcp/8800+ and should also be permitted in the firewall of the Proxmox server as well as the VLAN that the instances are hosted on. This should is already configured as part of the datacenter workflow. If you run a second external firewall like on a router, that will also need to be configured to permit traffic between vlans. 
### 🏗️ [[../Standards/Tofu Workflow|Tofu Workflow]]
Remote State Files are a part of the infrastructure that **relies on an external service**. Unlike an AWS DynamoDB with S3, or an Azure File Storage solution, GitLab is a free resource for storing a remote state file using HTTP REST methods. There is an option to use [local state files](https://developer.hashicorp.com/terraform/language/settings/backends/local)instead of the remote state file. With this option the state file is stored on your localhost to disk in the form of a *.json* file in your projects root directory. It is possible to deploy all of this by using local state files, you will just need to modify the data blocks to point to the new state files; a change you will need to make to the code already.

<<[[Dependencies]] --- [[Deployment]]>>