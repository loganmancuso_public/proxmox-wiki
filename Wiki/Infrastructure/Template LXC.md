---
repo_id: 58634482
type: how-to
---
## 📸 Summary

This workflow deploys a template lxc container on all proxmox nodes. This template is bootstrapped with custom packages, including a non-root user with permissions for ssh. By default the root user will be disabled for ssh access in exchange for the standard user. The same directories and folder structure on the machine templates is replicated here.  
### 🗃️ [Repo](https://gitlab.com/loganmancuso_public/infrastructure/proxmox/template-lxc)
`git clone git@gitlab.com:loganmancuso_public/infrastructure/proxmox/template-lxc.git`
### 📜 Dependencies
- [[../Infrastructure/Global Secrets|Global Secrets]]
- [[Datacenter|Datacenter]]

### 📃 Sample TFvars
```tofu
config = {
  env = "dev"
}

image = {
  id          = XXXXXXX
  os          = "ubuntu"
  name        = "noble-2404"
  version     = "latest"
  unprivileged = true
  tags        = ["docker"]
  ip          = "XXX.XXX.XXX.XXX"
  network     = "network"
  description = "# Ubuntu LXC Template\n## Noble Image 24.04 with docker pre-installed"
}
```

---
## ⚙️ Deployment Instructions
#### 🛑 Pre-Deployment

#### 🟢 Deployment
to deploy this workflow link the environment variables. the image/os is pulled from a local variable with all the hashes and iso urls to make downloading and changing versions easier.

the file path for this symlink has a specific structure and it helps avoid having to use tofu workspaces. Gitlab does not yet support remote state files with workspaces so side along deployments need to be deployed under different state files. the env is the datacenter env that this template will belong to, the os will be the os common name (ubuntu, debian). The name references the name of the os version (noble-2404, bookwork1200). In my case I use 4 digits to help distinguish version numbers. Then under that is the revision, either latest or stable. This helps to have 2 simultaneous virtual machine templates deployed as you are working on new tweaks. taking down the  template means you cannot spawn new virtual machines. Too many times I have deleted the vm template to realize I cant get the automation to work again and im stuck without being able to deploy a new vm. The best way is to just deploy a stable that is always available, and then use the latest revision when working on new changes. 

```bash
ln -s env/{env}/{os}/{name}/{version}/* .
tofu init .
tofu plan
tofu apply
```
#### 🏁 Post-Deployment


---

## 📝 Notes


to destroy just the instance and redeploy 
```bash
tf destroy --target proxmox_virtual_environment_container.instance --auto-approve
```

remote into target instance

```bash
ssh root@xxx.xxx.xxx.xxx

tail -f /var/log/init.log

grep -e warning /var/log/init.log
```

on the instance the logs for the fist init bootstrapping can be found in `/var/log/init.log` and the transformed script at `/opt/init.sh` 

the module has a bootstrapping that runs after to customize the machine further. You do not need to make a template for any specific machine, instead just install default packages and configurations. Then deploy [[Module Machine]] to customize the deployment further. 


### 📅 Tasks
- [ ] 
### 👎 Known Issues
- 