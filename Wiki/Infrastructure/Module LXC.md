---
repo_id: 58875884
type: how-to
---
## 📸 Summary
tofu modules allow us to reuse common code in a more manageable way. The module picks up a [[Template LXC]] and adds customization on top of it. Using the pre-built image gives us a starting point for small tweaks to each type of container we intend to deploy. GitLab allows us to store these modules in a hosted repository and there is an included gitlab yaml workflow to publish the module. 

### 🗃️ [Repo](https://gitlab.com/loganmancuso_public/infrastructure/proxmox/module-lxc)
`git clone git@gitlab.com:loganmancuso_public/infrastructure/proxmox/module-lxc.git`
### 📜 Dependencies
- [[../Infrastructure/Global Secrets|Global Secrets]]
- [[Datacenter|Datacenter]]
- [[Template LXC]]

### 📃 Sample Main.tf
since this is a module testing will not be done against the root module but instead will be run within the env folders. This will also only have a main.tf 
```tofu
## Provider ##
terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/XXXXXXXX/terraform/state/env-module-lxc"
  }
}

locals {
  config = {
    env = "env"
  }
  instance = {
    network  = "mgmt"
    ip_start = "XXX.XXX.XXX.XXX"
    version  = "stable" # latest or stable (version shema from the template workflow)
  }
}

module "container" {
  for_each = { for idx, key in keys(local.nodes) : key => idx }
  source   = "../"

  providers = {
    proxmox = proxmox
  }

  config = {
    env = local.config.env
    # node = "worker01"
    node      = each.key
    subdomain = "test"
  }


  instance = {
    unprivileged  = true
    start_on_boot = true
    # name          = "module-0"
    name        = "module-${each.key}"
    description = "test lxc module"
    template    = "debian-bookworm-1207-${local.instance.version}"
    # id            = 10
    id      = 10 + each.value
    network = local.instance.network
    # ip            = local.instance.ip_start
    ip        = "${split(".", local.instance.ip_start)[0]}.${split(".", local.instance.ip_start)[1]}.${split(".", local.instance.ip_start)[2]}.${split(".", local.instance.ip_start)[3] + each.value}"
    cpu_cores = 2
    memory    = 2048
    disk_size = 32
    mount_point = []
    tags = ["module-test"]
  }

  security_group_rules = {
    inbound = [
      {
        action  = "ACCEPT"
        comment = "default ${local.networks["mgmt"].name} access"
        source  = "dc/${local.networks["mgmt"].id}"
        proto   = "tcp"
        port    = null
        log     = "alert"
      }
    ]
    outbound = []
  }

  bootstrap = {
    path = "./scripts/bootstrap.sh"
    tokens = {
      new = "test"
    }
  }

}

output "module" {
  # value = module.container
  value = { for idx, key in keys(local.nodes) : key => module.container[key] }
}

```

---
## ⚙️ Deployment Instructions
#### 🛑 Pre-Deployment
set the secret values for communicating with the gitlab api. this is responsible for storing the module in the remote gitlab storage. 

```bash
# Proxmox #  
export TF_TOKEN_gitlab_com=glpat-XXXXXXXXXXXXXXXXXXXXXXXXXXXX  
export TF_HTTP_USERNAME={gitlab_username}
```

testing this workflow locally is best before publishing the module. Since this is a module we do not deploy the root folder but instead use the env folder to test the deployment.
```bash
cd env/
tofu init .
tofu plan
tofu apply
```

#### 🟢 Deployment
1. navigate to gitlab and open the project
2. under build open pipelines [here](https://gitlab.com/loganmancuso_public/infrastructure/proxmox/module-lxc/-/pipelines)
3. run the pipeline providing the variable CI_COMMIT_TAG with the value being the version you intend to deploy X.Y.Z (1.0.0)
4. you can see the published module [here](https://gitlab.com/loganmancuso_public/infrastructure/proxmox/module-lxc/-/terraform_module_registry)
#### 🏁 Post-Deployment


---

## 📝 Notes


```bash
docker run --entrypoint bash --rm -w $PWD -v $PWD:$PWD -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:v15.11.1 -c 'git config --global --add safe.directory "*";gitlab-runner exec docker upload'
```

### 📅 Tasks
- [ ] 
### 👎 Known Issues
- .

