---
repo_id: 48941316
type: how-to
---
## 📸 Summary

This workflow deploys a simple test instance to proxmox, its default security group allows all outbound traffic, unless interfering with the datacenter or default vm rules. This is designed to get an instance deployed quickly to a node for testing. 
### 🗃️ [Repo](https://gitlab.com/loganmancuso_public/infrastructure/proxmox/instances/sandbox)
`git clone git@gitlab.com:loganmancuso_public/infrastructure/proxmox/instances/sandbox.git`
### 📜 Dependencies
- [[../Module Machine|Module Machine]]
- [[../Datacenter|Datacenter]]
- [[../Global Secrets|Global Secrets]]

### 📃 Sample TFvars
```tofu
config = {
  env = "env"
  node = "nodeid"
}

instance_settings = {
	id = XXX
	ip = "XXX.XXX.XXX.XXX"
	template = "mantic-2310-{version}" # this is the packer vm template tf state
}
```

---
## ⚙️ Deployment Instructions
#### 🛑 Pre-Deployment


#### 🟢 Deployment

To deploy this workflow link the environment folder to the root directory.
```bash
ln -s env/sbx#/* .
tofu init .
tofu plan
tofu apply
```

#### 🏁 Post-Deployment


---

## 📝 Notes
- 

### 📅 Tasks
- [ ] 
### 👎 Known Issues
- [ ] 

<< --- [[../Template Machine|Template Machine]]>>