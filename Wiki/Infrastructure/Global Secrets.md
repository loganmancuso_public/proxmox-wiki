---
repo_id: 52104036
type: how-to
---
## 📸 Summary

This workflow will deploy the paths and configuration of the Vault. This organizes the secrets into groups of alike secrets for easier access control management. To see what groups are deployed in the vault check here [[../Standards/Secrets|Secrets]] 
### 🗃️ [Repo](https://gitlab.com/loganmancuso_public/infrastructure/proxmox/global-secrets)
`git clone git@gitlab.com:loganmancuso_public/infrastructure/proxmox/global-secrets.git`
### 📜 Dependencies
- [[../Applications/Vault|Vault]]

### 📃 Sample TFvars

```tofu
dns_names = [
"example.com",
"*.example.com",
"xxx.xxx.xxx.xxx/yy",
]
  
## Shared ##
ssh = {
	key_private_path = "~/.ssh/id_ed25519"
	key_public = "ssh-ed25519 XXXXXXXXXXXXXXXXx"
}
  
proxmox = {
	username = "root"
	password = "mysecretpassword"
}

# this is something i have baked into the automation since I use an external mail provider, however you may want to remove this. In datacenter workflow, you can just remove the import from imports.tf and the bootstrapping.sh. If you do want to configure this it will setup email notifications automatically for each node. 
smtp = {
  host     = "smtp.gmail.com"
  port     = "587"
  username = "my@gmail.com"
  password = "XXXXXXXXXXX"
}


# this can be added after datacenter is deployed, I am going to migrate this insert into the datacenter workflow at some point
the api_key map has a set of keys (dev, prod) those need to match the datacenter env config value. everything is indexed off of that, so if you have deployed datacenter make sure to set that here
user_operations = {
	username = "operations@pve"
	password = "password"
	api_id = "someuser@pve!packer"
	api_key = {
		dev = "apitoken from proxmox datacenter"
		prod = "apitoken from proxmox datacenter"
	}
}
# this is for remote authentication into instance's (vm/lxc). these credentials will be passed to the template-machine/lxc automation when building an instance template. this is how you will be able to control root level access to the instances. After that they could be joined to an ldap for identity based login. 
instance = {
	username = "user"
	password = "password"
	hashed_password = "$"
	salt = "some salt"
}
```

---
## ⚙️ Deployment Instructions
#### 🛑 Pre-Deployment

-  `mkpasswd -m md5 -S some-salt some-password`
- If not unlocked already, unlock the vault by either running the commands below or use the helper script in the vault project under ./scrips/unseal.sh
```bash
# Unlocking the exisiting vault
vault operator unseal $UNSEAL_KEY_1
vault operator unseal $UNSEAL_KEY_2
vault operator unseal $UNSEAL_KEY_3
vault login $VAULT_DEV_ROOT_TOKEN_ID
vault status -format=json
```

#### 🟢 Deployment

To deploy this workflow link the environment folder to the root directory.
```bash
ln -s env/* .
tofu init .
tofu plan
tofu apply
```

#### 🏁 Post-Deployment


---

## 📝 Notes

### Backup Method

I go over some of the practices to backup the vault where these secrets are stored however its not necessary as long as you retain a copy of the tfvars from this workflow. all of the automation can be redeployed to a new system so long as this workflow deploys the same secrets to the vault. In my case I do backup the vault docker container volume and the certs, keys, and config file. However, when I migrate to a new system (format my personal device). I just redeploy the vault and global secrets from scratch again and just copy over the tfvar file to this workflow. It makes the entire process a lot easier having to only backup a single file.  

### 📅 Tasks
- [ ] 
### 👎 Known Issues
- [ ] 

<<[[../Applications/Vault|Vault]] --- [[Datacenter|Datacenter]]>>