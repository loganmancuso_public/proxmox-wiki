---
repo_id: 52530007
type: how-to
---
## 📸 Summary

Vault is an application that houses secrets at-rest for use later on. When building applications you need to decouple the source code from the secrets that are needed to configure an application. For more information about the vault please look at the [docs](https://developer.hashicorp.com/vault). 

My original method of deploying vault was to use docker compose. After more research I discovered that to run vault in a proper deployment it would need some kind of key system to back it. I could have done kubernetes but i wanted something that anyone could do, so I chose to do certificates. This workflow creates and manages a set of keys and certs to encrypt and validate the vault server. If you prefer to manage your own certs or have a cert available to you for use then docker-compose might be a better solution for your setup. My goal was portability and keeping all of the logic as close together, however that isnt always necessary in certain environments. 
### 🗃️ [Repo](https://gitlab.com/loganmancuso_public/infrastructure/applications/vault)
`git clone git@gitlab.com:loganmancuso_public/infrastructure/applications/vault.git`
### 📜 Dependencies
- none

### 📃 Sample TFvars
```tofu
cert_subject = {
	common_name = "common_name"
	organization = "organization"
	country = "country"
	locality = "locality"
	postal_code = "zipcode"
	province = "state"
}
```

### 🍱 Containers
- hashicorp vault: this is the vault application that allows you to store secrets at rest for use in automations. I have a plan to migrate this to [openbao](https://github.com/openbao/openbao) as a more stable long term solution.

---
## ⚙️ Deployment Instructions
#### 🛑 Pre-Deployment


#### 🟢 Deployment

To deploy this workflow link the environment folder to the root directory.
```bash
ln -s env/* .
tofu init .
tofu plan
tofu apply
```

#### 🏁 Post-Deployment
1. import the root cert to a chrome browser chrome://settings/certificates
2. Initializing the vault, these values will only be shown once on creation do no lose these! You may also increase the key share count for more security but I did not see a need for my setup.
```bash
export VAULT_ADDR='https://localhost:8200'
export VAULT_CAPATH=/etc/ssl/certs/${var.cert_subject.common_name}.pem 
# Generate the Sealing keys
vault operator init -key-shares=3 -key-threshold=2
```
2. Set the environment variables, in my case I have a proxmox.env file that i store these secrets in. (check notes for details)
```bash
export VAULT_DEV_ROOT_TOKEN_ID={root token from previous command}
export UNSEAL_KEY_1={key num1 from the previous command}
export UNSEAL_KEY_2={key num2 from the previous command}
export UNSEAL_KEY_3={key num3 from the previous command}
```
3. There is a helper script under scripts/unseal.sh that will take your environment variables exported above and unseal the vault. This will need to be run each time the localhost is rebooted. 

---

## 📝 Notes

- check that the Root Cert is trusted on the local machine, this is part of the automation already
```bash
"sudo cp ${local_file.root["ca"].filename} /etc/ssl/certs/${var.cert_subject.common_name}.pem",
"sudo update-ca-certificates"
```
- Add the unlock tokens to a proxmox.env file and source the file using `source proxmox.env` the blanked values should be part of the output from the `vault operator init` command. The TF_HTTP_PASSWORD is the api token for storing remote states in gitlab. 
```bash
#!/bin/bash  
echo "setting proxmox secrets"  
# Proxmox #  
export TF_HTTP_PASSWORD=glpat-XXXXXXXXXXXXXXXXXXXXXXXXXXXX  
export TF_TOKEN_gitlab_com=glpat-XXXXXXXXXXXXXXXXXXXXXXXXXXXX  
export TF_HTTP_USERNAME={gitlab_username}
# Hashi Vault #  
export VAULT_ADDR='https://localhost:8200'  
export VAULT_CAPATH=/etc/ssl/certs/${var.cert_subject.common_name}.pem  
export VAULT_DEV_ROOT_TOKEN_ID=hvs.XXXXXXXXXXXXXXXXXXXXXX
export UNSEAL_KEY_1=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
export UNSEAL_KEY_2=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
export UNSEAL_KEY_3=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

### Backup

this workflow will house the vault for all of the downstream secrets. It would be useful to backup this docker container as well as the tfvars from the [[../Infrastructure/Global Secrets|Global Secrets]] workflow as well. A few important things to backup would be the keys for securing the vault as well as the app folder with the certificates and configuration hcl file. You can also backup the docker volume, but that will vary based on how you have docker configured. In the default scenario it will be stored at `/var/lib/docker/volumes` under which you should see a volume called `data-vault`. Backup this volume to ensure the vault can be shifted to another system. An alternate backup method is explained in the [[../Infrastructure/Global Secrets|Global Secrets]] workflow




##### 🔌 Ports

| internal | external | protocol |
| -------- | -------- | -------- |
| 80       | 80       | tcp      |


### 📅 Tasks
- [ ] 

### 👎 Known Issues
- [ ] 

<<[[../How-To/Deployment|Deployment]] --- [[../Infrastructure/Global Secrets|Global Secrets]]>>