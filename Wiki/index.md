---
type: index
---
## 🔉Introduction

Hosting applications at home can be a great way to control your data and save money on application subscriptions. However, managing on-premise infrastructure can be cumbersome, and if left unattended, a liability. I set out to find a better way to manage these resources.

---

<H3 style="text-align: center;">💡 Problem Statement</H3>
<p style="text-align: center;">How can I replicate the experience of deploying applications to a cloud provider, at home, with minimal cost?</p>

---

As an IaC developer I use [Terraform](https://www.terraform.io/) and its open source fork [OpenTofu](https://opentofu.org/), to manage scaled infrastructure on the cloud, and wanted to mimic this methodology at home. I have been using Proxmox for about 10 years now and have grown to love the scalability of it. I was doing research in to the proxmox API trying to find a way to automate and provision infrastructure hrough programmatic means, It was at this point I found a [Terraform provider](https://registry.terraform.io/providers/bpg/proxmox/latest) for the Proxmox api.

Leveraging this provider and opentofu I set out to build a modular workflow that would deploy the minimum infrastructure necessary for a secure proxmox datacenter. I wanted the automation to take a blank install of proxmox on a fresh machine and bring it up with all the customization I would need to host instances upon. 

➡️ Option 1   
If you would like to learn more about IaC please visit my [[How-To/IaC|IaC]] page.

➡️ Option 2   
If you are comfortable with IaC feel free to start looking at the [[How-To/Dependencies|Dependencies]] page. 
 
---

## 🏳️ TLDR: 
if you are comfortable with IaC follow these steps [[How-To/Start|here]] to get started deploying this infrastructure.

--- 
## Author [[Mentions]]