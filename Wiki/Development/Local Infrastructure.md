---
repo_id: 56137667
type: local
---
## 📸 Summary

this workflow deploys a libvirt network and basic infrastructure to deploy a libvirt instance. passing the network status as an output to the libvirt instances
### 🗃️ [Repo](https://gitlab.com/loganmancuso_public/infrastructure/local/local-infrastructure)
`git clone git@gitlab.com:loganmancuso_public/infrastructure/local/local-infrastructure.git`

### 📜 Dependencies


### 📃 Sample TFvars
```tofu
network_settings = {
  name        = "lan"
  mode        = "nat"
  cidr        = "XXX.XXX.XXX.XXX/YY"
  gateway     = "XXX.XXX.XXX.XXX"
  dns_servers = ["XXX.XXX.XXX.XXX", "XXX.XXX.XXX.XXX", "XXX.XXX.XXX.XXX"]
}
```

---
## ⚙️ Deployment Instructions
#### 🛑 Pre-Deployment


#### 🟢 Deployment

to deploy this workflow link the environment tfvars folder to the root directory.
```bash
ln -s env/* .
tofu init .
tofu plan
tofu apply
```
  
#### 🏁 Post-Deployment

- the instance ip will not be available on first deploy with dhcp but a `terraform refresh` will fix the issue.

---

## 📝 Notes
- .

### 📅 Tasks
- [ ] 

### 👎 Known Issues
- [ ] 

<<[[../Infrastructure/Global Secrets|Global Secrets]] --- [[Proxmox Dev]]>>