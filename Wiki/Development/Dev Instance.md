---
repo_id: 56359401
type: local
---
## 🚧 UNDER CONSTRUCTION 🚧 ##

this is an experimental approach, i am still building this workflow 😀
## 📸 Summary

This workflow deploys the infrastructure instance that hosts the gitlab runner for ci/cd deployments.
### 🗃️ [Repo](https://gitlab.com/loganmancuso_public/infrastructure/local/infra-instance)
`git clone git@gitlab.com:loganmancuso_public/infrastructure/local/dev-instance.git`
### 📜 Dependencies
- [[Local Infrastructure]]
- [[../Infrastructure/Global Secrets|Global Secrets]]

### 📃 Sample TFvars
```tofu
instance_settings = {
  name      = "infra-instance"
  ip        = "XXX.XXX.XXX.XXX"
  disk_size = 15
}
```

---
## ⚙️ Deployment Instructions
#### 🛑 Pre-Deployment


#### 🟢 Deployment

To deploy this workflow link the environment folder to the root directory.
```bash
ln -s env/* .
tofu init .
tofu plan
tofu apply
```

#### 🏁 Post-Deployment


---

## 📝 Notes

```bash
sudo virsh pool-list
sudo virsh pool-destroy default
sudo virsh pool-destroy Downloads
sudo virsh pool-undefine infra-instance
sudo virsh undefine infra-instance
sudo virsh domstate infra-instance
```
- to destroy just the instance use this command `tf destroy --target libvirt_domain.instance`


### 📅 Tasks
- https://docs.gitlab.com/runner/install/docker.html
- https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-the-docker-image-docker-in-docker
- https://docs.gitlab.com/ee/user/packages/dependency_proxy/index.html#use-the-dependency-proxy-for-docker-images
- https://docs.gitlab.com/ee/user/packages/container_registry/
- https://docs.gitlab.com/ee/tutorials/create_register_first_runner/
### 👎 Known Issues
- [ ] 

<<[[Local Infrastructure]] --- [[Proxmox Dev]]>> 