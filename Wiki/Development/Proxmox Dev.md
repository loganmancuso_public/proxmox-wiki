---
repo_id: 56137773
type: local
---
## 🚧 UNDER CONSTRUCTION 🚧 ##

this is an experimental approach, i am still building this workflow 😀
## 📸 Summary


This workflow deploys the dev environment for proxmox, this allows testing of tofu automation before deploying to prod.
### 🗃️ [Repo](https://gitlab.com/loganmancuso_public/infrastructure/local/proxmox-dev)
`git clone git@gitlab.com:loganmancuso_public/infrastructure/local/proxmox-dev.git`

### 📜 Dependencies
- [[Local Infrastructure]]
- [[../Infrastructure/Global Secrets|Global Secrets]]

### 📃 Sample TFvars
```tofu
instance_settings = {
  name      = "proxmox-dev"
  ip        = "XXX.XXX.XXX.XXX"
  disk_size = 60
  iso       = "proxmox-ve_8.1-2.iso"
}
```

---
## ⚙️ Deployment Instructions
#### 🛑 Pre-Deployment

configure your local user to deploy vms by adding them to the libvirt group. 
https://lunux.net/use-virt-manager-as-a-non-root-user-on-linux/
https://github.com/dmacvicar/terraform-provider-libvirt/issues/978#issuecomment-1276244924

```bash
sudo usermod -a -G libvirt $(whoami)
newgrp libvirt
# to uncomment these lines in the conf 
sudo sed -i 's/^# *\(unix_sock_group = "libvirt"\)/\1/' /etc/libvirt/libvirtd.conf
sudo sed -i 's/^# *\(unix_sock_rw_perms = "0770"\)/\1/' /etc/libvirt/libvirtd.conf
sudo sed -i 's/#security_driver = "selinux"/security_driver = "none"/' /etc/libvirt/qemu.conf

sudo chown -R libvirt-qemu:libvirt /var/lib/libvirt/images/
# sudo chmod 770 /var/lib/libvirt/images
sudo chown -R libvirt-qemu:libvirt /var/lib/virt/images/
```

download the proxmox iso and put it into a folder at the root called `iso` I was going to add in the download of the iso to the workflow but its too variable. I would also need to put a life-cycle prevent destroy on the iso resource and would make the entire workflow more difficult to use as I am constantly spinning up and tearing down this vm for testing. You will also need to copy that iso to `/var/lib/libvirt/images/` in order for libvirt to have permissions to it. 

```bash
rsync -v iso/*.iso /var/lib/libvirt/images/
```

#### 🟢 Deployment
to deploy this workflow link the environment tfvars folder to the root directory.

```bash
ln -s env/* .
tofu init .
tofu plan
tofu apply
```

#### 🏁 Post-Deployment
after this proxmox node is stood up you will need to do a few more things

1.  copy ssh key to the proxmox node `ssh-copy-id -i ~/.ssh/id_ed25519 root@100.64.0.10`
2. change the root password in the gui
3. you will need to run the [[../Infrastructure/Datacenter|Datacenter]] deployment on this newly deploy proxmox node and follow the post deployment instructions there as well. 


---

## 📝 Notes
- 
### 📅 Tasks
- [ ] 
### 👎 Known Issues
- [x] the instance ip will not be available on first deploy with dhcp but a `terraform refresh` will fix the issue.
- [ ] issue deleting the storage pool on a bad deployment with cloud_init. when deploying if vm becomes in an unusable state when deploying try these commands to clear the state and start fresh. 
```bash
sudo virsh pool-list
sudo virsh pool-destroy default
sudo virsh pool-destroy Downloads
sudo virsh pool-undefine proxmox-dev
sudo virsh undefine proxmox-dev
sudo virsh domstate proxmox-dev
```
- to destroy just the instance use this command `tf destroy --target libvirt_domain.instance`

expand disk for the vm

```bash
qemu-img resize --preallocation=full /var/lib/virt/images/proxmox-dev/os_image_proxmox +XG
```

<<[[Local Infrastructure]] --- [[../Infrastructure/Datacenter|Datacenter]]>> 